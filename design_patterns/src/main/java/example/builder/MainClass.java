package example.builder;

public class MainClass {

    public static void main(String[] args) {
        Student student = new Student.Builder()
                .id(1L)
                .name("Nurlan")
                .surname("Bashirov")
                .build();

        System.out.println("Student = " + student);
    }

}
