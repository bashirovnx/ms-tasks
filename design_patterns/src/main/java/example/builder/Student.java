package example.builder;

public class Student {

    private Long id;
    private String name;
    private String surname;

    public static class Builder {

        private Long id;
        private String name;
        private String surname;

        public Builder id(Long id) {
            this.id = id;

            return this;
        }

        public Builder name(String name) {
            this.name = name;

            return this;
        }

        public Builder surname(String surname) {
            this.surname = surname;

            return this;
        }


        public Student build() {

            Student student = new Student();
            student.id = this.id;
            student.name = this.name;
            student.surname = this.surname;

            return student;
        }
    }

    private Student() {

    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

}