package example.factory;

public class AnimalFactory {

    public static Animals getAnimal(Class clazz) {

        if (Cats.class == clazz) {
            return new Cats();
        } else if (Dogs.class == clazz) {
            return new Dogs();
        } else {
            throw new IllegalArgumentException("Unsupported type");
        }
    }

}


