package example.factory;

public class MainClass {

    public static void main(String[] args) {
        Animals animals1 = AnimalFactory.getAnimal(Dogs.class);
        System.out.println("Name 1 = " + animals1.getName());

        Animals animals2 = AnimalFactory.getAnimal(Cats.class);
        System.out.println("Name 2 = " + animals2.getName());
    }

}