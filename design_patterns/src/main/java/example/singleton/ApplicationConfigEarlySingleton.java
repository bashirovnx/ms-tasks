package example.singleton;

public class ApplicationConfigEarlySingleton {

    private static ApplicationConfigEarlySingleton instance = new ApplicationConfigEarlySingleton();

    private String username = "bashirovnx";
    private String password = "**********";

    private ApplicationConfigEarlySingleton() {

    }

    public static ApplicationConfigEarlySingleton getInstance() {
        return instance;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
