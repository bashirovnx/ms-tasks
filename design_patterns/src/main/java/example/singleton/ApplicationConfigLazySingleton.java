package example.singleton;

public class ApplicationConfigLazySingleton {

    private static ApplicationConfigLazySingleton instance;

    private String username = "bashirovnx";
    private String password = "**********";

    private ApplicationConfigLazySingleton() {

    }

    public static ApplicationConfigLazySingleton getInstance() {

        if (instance == null) {
            synchronized (ApplicationConfigLazySingleton.class) {
                if (instance == null) {
                    instance = new ApplicationConfigLazySingleton();
                }
            }
        }
        return instance;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}