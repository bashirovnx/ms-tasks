package example.singleton;

public class MainClass {

    public static void main(String[] args) {

        ApplicationConfigEarlySingleton earlySingleton = ApplicationConfigEarlySingleton.getInstance();
        System.out.println("Early init. Username: " + earlySingleton.getUsername() + ", password" + earlySingleton.getPassword());

        ApplicationConfigLazySingleton lazySingleton = ApplicationConfigLazySingleton.getInstance();
        System.out.println("Lazy init. Username: " + lazySingleton.getUsername() + ", password" + earlySingleton.getPassword());


    }

}
