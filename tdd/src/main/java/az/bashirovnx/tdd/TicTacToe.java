package az.bashirovnx.tdd;

public class TicTacToe {

    private char[][] board = {
            {'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'}
    };

    private char lastPlayer;
    private static final int SIZE = 3;

    public String play(int x, int y) {
        checkXAxis(x);
        checkYAxis(y);
        checkBoardIsOccupiedThrownElseSetValue(x, y);
        lastPlayer = nextPlayer();

        // printTheBoard();
        if (isWin(x, y)) {
            return lastPlayer + " is the winner";
        } else if (isDraw()) {
            return "The result is draw";
        } else {
            return "No winner";
        }

    }


    private void checkXAxis(int x) {
        if (x < 1 || x > 3) {
            throw new RuntimeException("X is outside board");
        }
    }

    private void checkYAxis(int y) {
        if (y < 1 || y > 3) {
            throw new RuntimeException("Y is outside board");
        }
    }

    private void printTheBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    public char nextPlayer() {
        if (lastPlayer == 'X') {
            return 'O';
        }
        return 'X';
    }

    private void checkBoardIsOccupiedThrownElseSetValue(int x, int y) {
        if (board[x - 1][y - 1] != '-') {
            throw new RuntimeException("Board is occupied");
        } else {
            board[x - 1][y - 1] = nextPlayer();
        }
    }

    private boolean isWin(int x, int y) {
        int playerTotal = lastPlayer * 3;
        char horizontal, vertical, diagonal1, diagonal2;
        horizontal = vertical = diagonal1 = diagonal2 = '\0';
        for (int i = 0; i < SIZE; i++) {
            horizontal += board[i][y - 1];
            vertical += board[x - 1][i];
            diagonal1 += board[i][i];
            diagonal2 += board[i][SIZE - i - 1];
        }
        if (horizontal == playerTotal
                || vertical == playerTotal
                || diagonal1 == playerTotal
                || diagonal2 == playerTotal) {
            return true;
        }
        return false;
    }

    private boolean isDraw() {
        for (int x = 0; x < SIZE; x++) {
            for (int y = 0; y < SIZE; y++) {
                if (board[x][y] == '\0') {
                    return false;
                }
            }
        }
        return true;
    }

}
