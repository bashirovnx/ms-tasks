package az.bashiorvnx.tdd;

import az.bashirovnx.tdd.Calculator;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


public class CalculatorTest {

    //given a and b when add then c
    //give 6 and 7 when add then 12 - white box testing

    Calculator calculator = new Calculator();

    @Test
    public void givenAAndBWhenAddThenC() {
        int a = 5;
        int b = 6;

        int c = calculator.add(a, b);

        assertThat(c).isEqualTo(a + b);

    }

    @Test
    public void givenAAndBWhenAddThenC2() {
        int a = 5;
        int b = 7;

        int c = calculator.add(a, b);

        assertThat(c).isEqualTo(a + b);

    }

    @Test
    public void givenAAndBWhenSubtractThenC() {
        int a = 5;
        int b = 7;

        int c = calculator.subtract(a, b);

        assertThat(c).isEqualTo(a - b);

    }

    @Test
    public void givenAAndBWhenDivideThenC() {
        //Arrange
        int a = 10;
        int b = 2;

        //Act
        int c = calculator.divide(a, b);

        //Assert
        assertThat(c).isEqualTo(a / b);

    }

    @Test
    public void givenAAndBWhenDivideByZeroThenException() {
        //Arrange
        int a = 10;
        int b = 0;

        //Act and Assert
        assertThatThrownBy(() -> calculator.divide(a, b))
                .isInstanceOf(RuntimeException.class);

    }

}
