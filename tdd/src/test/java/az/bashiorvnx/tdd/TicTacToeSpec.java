package az.bashiorvnx.tdd;

import az.bashirovnx.tdd.TicTacToe;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class TicTacToeSpec {

    private TicTacToe ticTacToe = new TicTacToe();

   @BeforeEach
    void init() {
        System.out.println("Before Each");
        ticTacToe = new TicTacToe();
    }

    @Test
    public void whenXIsOutsideTheBoardThenException() {
        int x = -1;
        int y = 2;

        assertThatThrownBy(() -> ticTacToe.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("X is outside board");
    }

    @Test
    public void whenYIsOutsideTheBoardThenException() {
        int x = 1;
        int y = 5;

        assertThatThrownBy(() -> ticTacToe.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Y is outside board");
    }

    @Test
    public void whenOccupiedThenRuntimeException() {
        ticTacToe.play(2, 1);
        assertThatThrownBy(() -> ticTacToe.play(2, 1))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Board is occupied");
    }

    @Test
    public void whenOccupiedThenRuntimeException1() {
        ticTacToe.play(2, 1); //x
        ticTacToe.play(2, 3); //y
        assertThatThrownBy(() -> ticTacToe.play(2, 1))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Board is occupied");
    }

    @Test
    public void givenLastTurnWasOWhenNextPlayerThenX() {
        assertThat(ticTacToe.nextPlayer()).isEqualTo('X');
    }

    @Test
    public void givenLastTurnWasXWhenNextPlayerThenO() {
        ticTacToe.play(1, 2);//X
        assertThat(ticTacToe.nextPlayer()).isEqualTo('O');
    }

    /*@Test
    public void whenPlayThenNoWinner()
    {
        String actual = ticTacToe.play(1,2);
        assertThat(actual).isEqualTo("No winner");
    }*/

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinner() {
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 1); // X
        ticTacToe.play(2, 2); // O
        String actual = ticTacToe.play(3, 1); // X
        assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    public void whenPlayAndWholeVerticalLineThenWinner() {
        ticTacToe.play(2, 1); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(3, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        String actual = ticTacToe.play(1, 3); // O
        assertThat(actual).isEqualTo("O is the winner");
    }

    @Test
    public void whenPlayAndTopBottomDiagonalLineThenWinner() {
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 3); // O
        String actual = ticTacToe.play(3, 3); // O
        assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    public void whenPlayAndBottomTopDiagonalLineThenWinner() {
        ticTacToe.play(1, 3); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 2); // O
        String actual = ticTacToe.play(3, 1); // O
        assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        ticTacToe.play(1, 1);
        ticTacToe.play(1, 2);
        ticTacToe.play(1, 3);
        ticTacToe.play(2, 1);
        ticTacToe.play(2, 3);
        ticTacToe.play(2, 2);
        ticTacToe.play(3, 1);
        ticTacToe.play(3, 3);
        String actual = ticTacToe.play(3, 2);
        assertThat(actual).isEqualTo("The result is draw");
    }

}
